using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi;
using Backend.WebApi.Controllers;
using Backend.WebApi.Helpers;
using Backend.WebApi.Repositories;
using FakeItEasy;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace UnitTests.Controllers
{
    public class UsersControllerTests : BaseControllerTests
    {
        private const string DummySessionToken = "abcdefgh";
        
        private (IUserAuthenficiation IUserAuthenficiation, ISessionTokenRepository ISessionTokenRepository, AppDbContext AppDbContext) CreateDIArguments()
        {
            var userAuth = A.Fake<IUserAuthenficiation>();
            var sessionRepo = A.Fake<ISessionTokenRepository>();
            var context = this.CreateInMemoryDbContext();
            
            return (userAuth, sessionRepo, context);
        }

        private (IUserAuthenficiation IUserAuthenficiation, ISessionTokenRepository ISessionTokenRepository,
            AppDbContext AppDbContext, UsersController UsersController) CreateControllerWithDefaultArguments()
        {
            var (userAuth, sessionTokens, context) = this.CreateDIArguments();
            var controller = new UsersController(context, userAuth, sessionTokens);
            return (userAuth, sessionTokens, context, controller);
        }
        
        [Fact]
        public async Task Login_WithValidCredentials_ReturnsSessionToken()
        {
            var (userAuth, sessionTokens, _, controller) = this.CreateControllerWithDefaultArguments();
            var dummySesionToken = new SessionToken() {Value = DummySessionToken};
            A.CallTo(() => userAuth.Validate(A<LoginCredentials>.Ignored)).Returns(true);
            A.CallTo(() => sessionTokens.CreateNewUserToken(A<string>.Ignored)).Returns(dummySesionToken);

            var result = await controller.Login(new LoginCredentials());

            result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public async Task Login_WithInvalidCredentials_ReturnsBadRequest()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.Validate(A<LoginCredentials>.Ignored)).Returns(false);

            var result = await controller.Login(new LoginCredentials());

            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public async Task Login_WithNullArgument_ReturnsBadRequest()
        {
            var (_, _, _, controller) = this.CreateControllerWithDefaultArguments();

            var result = await controller.Login(null);

            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public async Task Logout_WithExistingSessionToken_ReturnsOkObjectResult()
        {
            const string api_key = "12345678";
            var (_, sessionTokens, _, controller) = this.CreateControllerWithDefaultArguments();
            controller.ControllerContext.HttpContext = this.CreateContext("POST", ("api_key", api_key));
            A.CallTo(() => sessionTokens.RemoveTokenByValue(api_key)).Returns(true);

            var result = await controller.Logout();

            result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public async Task Logout_WithNonExistingSessionToken_ReturnsNotFoundObjectResult()
        {
            const string api_key = "12345678";
            var (_, sessionTokens, _, controller) = this.CreateControllerWithDefaultArguments();
            controller.ControllerContext.HttpContext = this.CreateContext("POST", ("api_key", api_key));
            A.CallTo(() => sessionTokens.RemoveTokenByValue(api_key)).Returns(false);

            var result = await controller.Logout();

            result.Should().BeOfType<NotFoundObjectResult>();
        }

        [Fact]
        public async Task Create_WithSuccessfulSave_ReturnsOkResult()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.Add(A<LoginCredentials>.Ignored)).Returns(true);
            var credentials = new LoginCredentials();
            
            var result = await controller.Create(credentials);

            result.Should().BeOfType<OkResult>();
        }

        [Fact]
        public async Task Create_WithErrorSave_ReturnsBadRequest()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.Add(A<LoginCredentials>.Ignored)).Returns(false);
            var credentials = new LoginCredentials();
            
            var result = await controller.Create(credentials);

            result.Should().BeOfType<BadRequestResult>();
        }

        [Fact]
        public async Task Remove_WithSuccessfulDelete_ReturnsOkResult()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.Remove(A<string>.Ignored)).Returns(true);

            var result = await controller.Remove("a");

            result.Should().BeOfType<OkResult>();
        }

        [Fact]
        public async Task Remove_WithErrorDelete_ReturnsNotFound()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.Remove(A<string>.Ignored)).Returns(false);
            
            var result = await controller.Remove("a");

            result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async Task GetById_WithExistingUser_ReturnsJsonResult()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.GetById(A<int>.Ignored)).Returns(new User());
            
            var result = await controller.GetById(1);

            result.Should().BeOfType<JsonResult>();
        }

        [Fact]
        public async Task GetById_WithNonexistingUser_ReturnsNotFound()
        {
            var (userAuth, _, _, controller) = this.CreateControllerWithDefaultArguments();
            A.CallTo(() => userAuth.GetById(A<int>.Ignored)).Returns<User>(null);

            var result = await controller.GetById(1);

            result.Should().BeOfType<NotFoundResult>();
        }
    }
}