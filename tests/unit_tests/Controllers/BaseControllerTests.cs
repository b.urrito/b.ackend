using System.Collections.Generic;
using System.Linq;
using Backend.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace UnitTests.Controllers
{
    public abstract class BaseControllerTests
    {
        /// <summary>
        /// Erzeugt eine In-Memory-Datenbank für das Testing.
        /// </summary>
        /// <returns></returns>
        protected AppDbContext CreateInMemoryDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "DefaultTestingInMemoryDb")
                .Options;
            
            return new AppDbContext(options);
        }

        protected HttpContext CreateContext(string method, params (string Key, string Value)[] headers)
        {
            var context = new DefaultHttpContext();
            context.Request.Method = method;
            headers.ToList().ForEach(header => context.Request.Headers.Add(header.Key, header.Value));
            return context;
        }
    }
}