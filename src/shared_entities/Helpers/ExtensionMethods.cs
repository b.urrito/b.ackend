using System;

namespace Backend.SharedEntities.Helpers
{
    public static class ExtensionMethods
    {
        public static Int32 ToUTCUnixTimestamp(this DateTime dateTime)
        {
           return (Int32)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;  
        }
    }
}