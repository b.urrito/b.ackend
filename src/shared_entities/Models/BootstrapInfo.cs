using System;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Informationspaket für das Gerät welches alle relevanten Informationen enthält.
    /// </summary>
    public class BootstrapInfo
    {
        [JsonProperty(PropertyName = "id")]
        public Guid DeviceId { get; set; }
        /// <summary>
        /// Zukünftiger Name des Geräts.
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "name")]
        public string DeviceName { get; set; }
        /// <summary>
        /// Zukünftiger Ort dieses Geräts.
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "location")]
        public string DeviceLocation { get; set; }
        /// <summary>
        /// Api Key für dieses Gerät. Wird benötigt weil alle Endpunkte geschützt sind.
        /// </summary>
        public string ApiKey { get; set; }
        /// <summary>
        /// Zeitzone des Geräts in der üblichen Stringrepräsentation.
        /// </summary>
        public string TimeZone { get; set; }
    }
}