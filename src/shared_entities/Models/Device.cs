using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Anzeigegerät für einen Kalender.
    /// </summary>
    public class Device
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Name dieses Geräts, z.B. "Besprechungsraum #1".
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Ort an dem sich dieses Gerät befindet, z.B. "Prinzenallee 5, DUS".
        /// </summary>
        /// <value></value>
        public string Location { get; set; }
        /// <summary>
        /// Url (HTTP/S) zum iCal Kalender.
        /// </summary>
        /// <value></value>
        public string Calendar { get; set; }
        /// <summary>
        /// Zeit zu der der Kalender dieses Geräts das letzte mal abgefragt wurde.
        /// </summary>
        /// <value></value>
        public DateTime LastActive { get; set; }
        /// <summary>
        /// Liste aller aufgetretenen Fehler an diesem Gerät.
        /// </summary>
        public IList<Error> Errors { get; set; } = new List<Error>();
        /// <summary>
        /// Name der Zeitzone dieses Geräts.
        /// </summary>
        public string TimeZone { get; set; }
        /// <summary>
        /// Zeitzone des Geräts als Instanz der TimeZoneInfo des .Net Frameworks.
        /// </summary>
        [JsonIgnore] public TimeZoneInfo TimeZoneInfo => 
            string.IsNullOrWhiteSpace(this.TimeZone) ?
                TimeZoneInfo.Utc : 
                TimeZoneInfo.FindSystemTimeZoneById(this.TimeZone);

    /// <summary>
    /// Prüft ob alle Daten welche für das Persistieren des Geräts in der DB vorhanden sind.
    /// </summary>
    /// <returns>Das persistierte Device (mit gesetzter Id).</returns>
        public bool IsMinimalDataSet()
        {
            return string.IsNullOrWhiteSpace(Name) == false &&
                   string.IsNullOrWhiteSpace(Location) == false &&
                   string.IsNullOrWhiteSpace(Calendar) == false;
        }

        /// <summary>
        /// Prüft ob die in dieser Instanz enthaltenen Informationen ausreichen
        /// um ein Gerät upzudaten.
        /// </summary>
        /// <returns></returns>
        public bool CanBeUpdated()
        {
            return this.Id != Guid.Empty;
        }
    }
}