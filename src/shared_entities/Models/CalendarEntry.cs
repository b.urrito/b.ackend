using System;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Backend.SharedEntities.Helpers;

namespace Backend.SharedEntities.Models
{
    public class CalendarEntry
    {
        /// <summary>
        /// Name des Kalendereintrags.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Startzeitpunkt (UNIX Timestamp UTC), berechnet sich aus <see cref="StartDateTime"/>.
        /// </summary>
        public int Start => StartDateTime.ToUTCUnixTimestamp();
        /// <summary>
        /// Startzeitpunkt als reguläres DateTime-Objekt.
        /// </summary>
        [JsonIgnore]
        public DateTime StartDateTime { get; set; }
        /// <summary>
        /// Endzeitpunkt (UNIX Timestamp UTC)
        /// </summary>
        public int End => EndDateTime.ToUTCUnixTimestamp();
        /// <summary>
        /// Endzeitpunkt als reguläres DateTime-Objekt.
        /// </summary>
        [JsonIgnore]
        public DateTime EndDateTime { get; set; }
        /// <summary>
        /// Termindauer in Minunten.
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// Ersteller des Termins.
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// Anzahl Teilnehmer.
        /// </summary>
        public int Participants { get; set; }
    }
}