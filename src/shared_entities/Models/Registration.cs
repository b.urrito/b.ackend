using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Registrierung eines neuen Geräts. Wird nach Anlegen eines Geräts wieder gelöscht.
    /// </summary>
    public class Registration
    {
        public int Id { get; set; }
        /// <summary>
        /// Name des Geräts.
        /// </summary>
        /// <value></value>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Standort des Geräts.
        /// </summary>
        /// <value></value>
        [Required]
        public string Location { get; set; }
        /// <summary>
        /// HTTP(S) Url des iCal Kalenders.
        /// </summary>
        /// <value></value>
        [Required]
        public string Calendar { get; set; }
        /// <summary>
        /// Zu dieser Registrierung gehöriger AuthToken.
        /// </summary>
        /// <value></value>
        public AuthToken AuthToken { get; set; }
        /// <summary>
        /// Name der Zeitzone dieses Geräts.
        /// </summary>
        public string TimeZone { get; set; }
        /// <summary>
        /// Zeitzone des Geräts als Instanz der TimeZoneInfo des .Net Frameworks.
        /// </summary>
        [JsonIgnore] public TimeZoneInfo TimeZoneInfo =>
            string.IsNullOrWhiteSpace(this.TimeZone) ? null : TimeZoneInfo.FindSystemTimeZoneById(this.TimeZone);
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Prüft ob alle notwendigen Daten zum Persistieren dieser Registrierung gesetzt sind.
        /// </summary>
        /// <returns></returns>
        public bool IsMinimalDataSet()
        {
            return string.IsNullOrWhiteSpace(Name) == false &&
                   string.IsNullOrWhiteSpace(Location) == false &&
                   string.IsNullOrWhiteSpace(Calendar) == false &&
                   string.IsNullOrWhiteSpace(TimeZone) == false;
        }

        /// <summary>
        /// Prüft ob es sich bei der URL um eine HTTP(S) Url handelt.
        /// </summary>
        /// <returns></returns>
        public bool ValidateCalendarUrl()
        {
            if (Calendar.StartsWith("http"))
                return true;
            else
                return false;
        }
    }
}