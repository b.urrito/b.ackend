namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Farben welche das eInk Display anzeigen kann.
    /// </summary>
    public enum LineColors
    {
        Black = 0,
        White = 1,
        Yellow = 2
    }

    /// <summary>
    /// Vorgefertigte Zeile welche auf dem Display angezeigt wird.
    /// </summary>
    public class PrerenderedCalendarLine
    {
        public const LineColors DefaultBackgroundColor = LineColors.White;
        public const LineColors DefaultForegroundColor = LineColors.Black;

        public LineColors ForegroundColor { get; set; } = DefaultForegroundColor;
        public LineColors BackgroundColor { get; set; } = DefaultBackgroundColor;
        public string Content { get; set; }

        public PrerenderedCalendarLine()
        {
            // für Deserialisierung
        }
        
        public PrerenderedCalendarLine(string content)
        {
            this.Content = content;
        }

        public PrerenderedCalendarLine(string content, LineColors foreground, LineColors background)
            : this(content)
        {
            this.ForegroundColor = foreground;
            this.BackgroundColor = background;
        }
    }
}
