using System.ComponentModel.DataAnnotations;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Fasst die für einen Login erforderlichen Daten zusammen.
    /// </summary>
    public class LoginCredentials
    {
        [Required] public string Username { get; set; } = string.Empty;
        [Required] public string Password { get; set; } = string.Empty;

        public LoginCredentials()
        {
            //
        }

        public LoginCredentials(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
        
        public bool IsMinimalDataset()
        {
            return string.IsNullOrWhiteSpace(this.Username) == false &&
                   string.IsNullOrWhiteSpace(this.Password) == false;
        }
    }
}