using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Backendbenutzer. Hat keinerlei Berechtigungen.
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        /// <summary>
        /// Name des Benutzers.
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Hash des Passworts.
        /// </summary>
        /// <value></value>
        [JsonIgnore]
        public string HashedPassword { get; set; }

        /// <summary>
        /// Liefert zurück ob diese Instanz alle notwendigen Informationen enthält um persistiert werden zu können.
        /// </summary>
        /// <returns></returns>
        public bool IsMinimalDataSet()
        {
            return !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(HashedPassword);
        }
    }
}