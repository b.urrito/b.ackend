using System;

namespace Backend.SharedEntities.Models
{
    public class Error
    {
        public int Id { get; set; }
        /// <summary>
        /// Fehlerbeschreibung.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Gerät auf welchem der Fehler aufgetreten ist.
        /// </summary>
        public Device Device { get; set; }
        /// <summary>
        /// Id des Geräts auf welchem der Fehler aufgetreten ist.
        /// </summary>
        public Guid DeviceId { get; set; }
        /// <summary>
        /// Zeitpunkt zu dem der Fehler aufgetreten ist.:w
        /// </summary>
        public DateTime Timestamp { get; set; }

        public bool IsMinimalDataSet()
        {
            return this.DeviceId != System.Guid.Empty && !string.IsNullOrWhiteSpace(this.Description);
        }
    }
}