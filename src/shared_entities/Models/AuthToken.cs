using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Ein AuthToken wird verwendet um eine Registrierung abzuschließen. Diese Tokens
    /// sind kurz, haben aber auch nur eine geringe Lebensdauer. Sie sind genau einmal benutzbar.
    /// </summary>
    public class AuthToken
    {
        /// <summary>
        /// Standardgültigkeitsdauer eines Tokens.
        /// </summary>
        public const int DefaultLifeTimeInMinutes = 30;
        
        [JsonIgnore] public int Id { get; set; }
        /// <summary>
        /// Wert des Tokens. 
        /// </summary>
        /// <value></value>
        public int Value { get; set; }
        /// <summary>
        /// Zeitpunkt zu dem dieser Token erzeugt wurde.
        /// </summary>
        /// <value></value>
        [JsonIgnore] public DateTime GenerationDate { get; set; }
        /// <summary>
        /// Zeitpunkt zu dem dieser Token abläuft.
        /// </summary>
        /// <value></value>
        public DateTime ExpirationDate { get; set; }
        /// <summary>
        /// Zu diesem Token gehörige Registrierung.
        /// </summary>
        [JsonIgnore] public Registration Registration { get; set; }
        /// <summary>
        /// Id der zu diesem Token gehörigen Registrierung.
        /// Wird für die Auflösung innerhalb des EF benötigt.
        /// </summary>
        [JsonIgnore] public int RegistrationId { get; set; }
    }
}