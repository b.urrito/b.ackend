using System;

namespace Backend.SharedEntities.Models
{
    /// <summary>
    /// Resultat einer Benutzeranmeldung, welche es dem Benutzer erlaubt
    /// weitere Befehle an die API abzusetzen.
    /// </summary>
    public class SessionToken
    {
        public int Id { get; set; }
        /// <summary>
        /// Tatsächlicher Tokenwert.
        /// </summary>
        /// <value></value>
        public string Value { get; set; }
        /// <summary>
        /// Zeitpunkt der Erzeugung dieses Tokens.
        /// </summary>
        /// <value></value>
        public DateTime GenerationDate { get; set; }
        /// <summary>
        /// Gültigkeitsdauer dieses Tokens.
        /// </summary>
        /// <value></value>
        public DateTime ValidTill { get; set; }
        /// <summary>
        /// Benutzer für den dieser Token generiert wurde.
        /// Aktuell sind die Tokens _nicht_ nutzergebunden, da es keine Berechtigungen gibt.
        /// </summary>
        /// <value></value>
        public string GeneratedFor { get; set; }
    }
}