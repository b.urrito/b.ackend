using System.Net.Http;
using System.Threading.Tasks;

namespace Backend.WebApi.Helpers
{
    public interface IHttpDownloadHelper
    {
        Task<string> ReadContentFromUrl(string url);
    }
    
    public class HttpDownloadHelper : IHttpDownloadHelper
    {
        private HttpClient _client = new HttpClient();
        
        public async Task<string> ReadContentFromUrl(string url)
        {
            var content = await _client.GetStringAsync(url);
            return content;
        }
    }
}