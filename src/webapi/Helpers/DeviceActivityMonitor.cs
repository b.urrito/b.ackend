using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;

namespace Backend.WebApi.Helpers
{
    public interface IDeviceActivityMonitor
    {
        void CheckDevicesForActivity();
    }

    public class DeviceActivityMonitor : IDeviceActivityMonitor
    {
        private const int ActivityWarningInMinutes = 120;
        
        private readonly IDevicesRepository deviceRepo;
        private readonly IErrorsRepository errorRepo;
        
        public DeviceActivityMonitor(IDevicesRepository deviceRepo, IErrorsRepository errorRepo)
        {
            this.deviceRepo = deviceRepo;
            this.errorRepo = errorRepo;
        }
        
        public void CheckDevicesForActivity()
        {
            Console.WriteLine("Starte Prüfung der Geräteaktivität.");
            var inactiveDevices = this.deviceRepo.GetInactiveFor(ActivityWarningInMinutes).ToList();
            var errors = inactiveDevices.Select(d => new Error
                {
                    Device = d,
                    DeviceId = d.Id,
                    Description = $"Das Gerät hat sich seit {d.LastActive} nicht mehr gemeldet.",
                    Timestamp = DateTime.Now
                });

            int inactiveDeviceCount = errors.Count();
            string inactiveDeviceIds = string.Join(Environment.NewLine, inactiveDevices.Select(d => $"{d.Id} ({d.Name}/{d.Location})"));
            Console.WriteLine($"Es wurden {inactiveDeviceCount} inaktive{(inactiveDeviceCount == 1 ? "s" : "")} {(inactiveDeviceCount == 1 ? "Gerät" : "Geräte")} gefunden:");
            Console.WriteLine($"{inactiveDeviceIds}");
            this.errorRepo.CreateAllSync(errors);
        }
    }
}