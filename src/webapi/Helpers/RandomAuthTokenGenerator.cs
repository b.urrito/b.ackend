using System;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Generiert einen zufälligen Token aus einem bestimmten Intervall.
    /// </summary>
    public interface IRandomAuthTokenGenerator
    {
        int Max { get; set; }
        int Min { get; set; }
        int Next();
    }
    
    /// <summary>
    /// Generiert einen AuthToken unter Verwendung der Random-Funktion des Frameworks.
    /// </summary>
    public class SystemRandomAuthTokenGenerator : IRandomAuthTokenGenerator
    {
        private readonly Random random = new Random();

        public int Min { get; set; } = 100000;
        public int Max { get; set; } = 999999;
        
        public int Next()
        {
            return random.Next(this.Min, this.Max + 1);
        }
    }
}