using System;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Backend.WebApi.Helpers
{
    public interface ICryptoProvider
    {
        string HashPassword(string password, string salt);
    }
    
    public class CryptoProvider : ICryptoProvider
    {
        public string HashPassword(string password, string salt)
        {
            var rawSalt = Encoding.Default.GetBytes(salt);
            var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: rawSalt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 25000,
                numBytesRequested: 256 / 8));
            return hashed;
        }
    }
}