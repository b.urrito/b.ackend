using System;
using System.Collections.Generic;
using Backend.SharedEntities.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.WebApi.Helpers
{
    public class JsonCalendarEntryFormatter : CalendarEntryFormatter
    {
        public override IActionResult CreateLayout(IEnumerable<CalendarEntry> entries, Device device, Func<string, string> convertToEspCompatibleText)
        {
            var lines = this.FormatEntriesAsCalendarLines(entries, device, convertToEspCompatibleText);
            return new JsonResult(lines);
        }
    }
}