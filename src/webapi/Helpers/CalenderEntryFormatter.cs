using System;
using System.Collections.Generic;
using System.Linq;
using Backend.SharedEntities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language.Extensions;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Wandelt einen Liste von <see cref="Backend.SharedEntities.Models.CalendarEntry"/> in eine formartierte
    /// Ausgabe um.
    /// </summary>
    public abstract class CalendarEntryFormatter
    {
        public abstract IActionResult CreateLayout(IEnumerable<CalendarEntry> entries, Device device, Func<string, string> convertToEspCompatible);

        private TimeZoneInfo currentTimeZoneInfo = null;
        
        protected IEnumerable<PrerenderedCalendarLine> FormatEntriesAsCalendarLines(
            IEnumerable<CalendarEntry> entries,
            Device device,
            Func<string, string> convertToEspCompatibleText)
        {
            // Als Fallbackzeitzone wird Europe/Berlin gesetzt weil das da ist wo wir sind 8)
            this.currentTimeZoneInfo = device.TimeZoneInfo;
            if (this.currentTimeZoneInfo == null)
                this.currentTimeZoneInfo = TimeZoneInfo.FromSerializedString("Europe/Berlin");
            
            var lines = new List<PrerenderedCalendarLine>(16);
            
            // Header
            lines.Add(new PrerenderedCalendarLine(
                $"{device.Name} - {device.Location}",
                LineColors.White,
                LineColors.Black));

            // HEUTE
            lines.Add(new PrerenderedCalendarLine(
                $"Heute ({ConvertFromUtc(DateTime.UtcNow).ToString("dd.MM.")})",
                LineColors.Black,
                LineColors.Yellow));

            const int maxEntries = 6;
            var contentLines = entries
                        .OrderBy(e => e.StartDateTime)
                        .Take(maxEntries)
                        .SelectMany(e =>
                {
                    var first = new PrerenderedCalendarLine(
                        $"{ConvertFromUtc(e.StartDateTime).ToString("HH:mm")} - {ConvertFromUtc(e.EndDateTime).ToString("HH:mm")} | {e.Title}");
                    var second = new PrerenderedCalendarLine(
                        $"              | {e.Participants} Teilnehmer");
                    return new List<PrerenderedCalendarLine> { first, second };
                });
            lines.AddRange(contentLines);

            int entriesForToday = entries.Count(e => e.StartDateTime.Date == DateTime.Today);
            lines.Insert(2 + 2 * entriesForToday, new PrerenderedCalendarLine(
                $"Morgen ({(ConvertFromUtc(DateTime.Today) + TimeSpan.FromDays(1)).ToString("dd.MM.")})",
                LineColors.Black,
                LineColors.Yellow));
            
            if(entriesForToday == 0)
                lines.Insert(2, new PrerenderedCalendarLine("Keine Termine."));

            int entriesForTomorrow = entries.Count() - entriesForToday;
            if(entriesForTomorrow == 0)
                lines.Add(new PrerenderedCalendarLine("Keine Termine."));
            
            while(lines.Count < 15)
                lines.Add(new PrerenderedCalendarLine(""));
            
            lines.Add(new PrerenderedCalendarLine(
                      $"Zuletzt aktualisiert am {DateTime.Now.ToShortDateString()} um {DateTime.Now.ToString("HH:mm")}.",
                      LineColors.White,
                      LineColors.Black));
            lines.ForEach(l => l.Content = convertToEspCompatibleText(l.Content));

            return lines;
        }

        private DateTime ConvertFromUtc(DateTime date)
        {
            date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            return TimeZoneInfo.ConvertTimeFromUtc(date, this.currentTimeZoneInfo);
        }
    }
}