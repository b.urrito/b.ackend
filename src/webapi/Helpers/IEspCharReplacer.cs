namespace Backend.WebApi.Helpers
{
    public interface IEspCharReplacer
    {
        string ConvertToEspCompatible(string input);
    }
}