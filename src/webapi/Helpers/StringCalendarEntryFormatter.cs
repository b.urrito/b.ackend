using System;
using System.Collections.Generic;
using System.Linq;
using Backend.SharedEntities.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.WebApi.Helpers
{
    public class StringCalendarEntryFormatter : CalendarEntryFormatter
    {
        public override IActionResult CreateLayout(IEnumerable<CalendarEntry> entries, Device device, Func<string, string> convertToEspCompatible)
        {
            var lines = this.FormatEntriesAsCalendarLines(entries, device, convertToEspCompatible);
            var strings = lines.Select(l => $"{(int)l.ForegroundColor}{(int)l.BackgroundColor}{l.Content}");
            var content = String.Join(Environment.NewLine, strings);
            return new OkObjectResult(content);
        }
    }
}