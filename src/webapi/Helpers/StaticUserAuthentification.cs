using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Authentifizierung die mir hardgecodeten Benutzername/Passwortkombinationen arbeitet.
    /// </summary>
    public class StaticUserAuthentification : IUserAuthenficiation
    {
        private readonly SortedDictionary<string, string> StaticLogins = new SortedDictionary<string, string>()
        {
            { "admin", "Duesseldorf2018!" }
        };
        
        public Task<bool> Validate(LoginCredentials credentials)
        {
            return Task.Run(() =>
            {
                if (StaticLogins.ContainsKey(credentials.Username))
                    return StaticLogins[credentials.Username] == credentials.Password;
                else
                    return false;
            });
        }

        public Task<bool> Add(LoginCredentials credentials)
        {
            return Task.Run(() =>
            {
                if (this.StaticLogins.ContainsKey(credentials.Username))
                {
                    return false;
                }
                else
                {
                    this.StaticLogins.Add(credentials.Username, credentials.Password);
                    return true;
                }
            });
        }

        public Task<bool> Remove(string username)
        {
            return Task.Run(() =>
            {
                if (this.StaticLogins.ContainsKey(username))
                {
                    this.StaticLogins.Remove(username);
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        public Task<bool> DoesUsernameExist(string username)
        {
            return Task.Run(() => { return this.StaticLogins.ContainsKey(username); });
        }

        public IEnumerable<User> All()
        {
            var users = new List<User>(this.StaticLogins.Count);
            for (int i = 0; i < this.StaticLogins.Count; ++i)
            {
                var pair = StaticLogins.ElementAt(i);
                users.Add(new User { Id = i, Name = pair.Key, HashedPassword = pair.Value });
            }

            return users;
        }

        public Task<User> GetById(int id)
        {
            return Task.Run(() =>
            {
                if (this.StaticLogins.Count <= id)
                    return new User()
                    {
                        Id = id,
                        Name = this.StaticLogins.ElementAt(id - 1).Key, 
                        HashedPassword = this.StaticLogins.ElementAt(id - 1).Value
                    };
                else
                {
                    return null;
                }
            });
        }
    }
}