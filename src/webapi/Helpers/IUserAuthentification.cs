using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Helpers
{
    // TODO: so umbauen, dass dieses Interface vom UsersRepository konsumiert wird un dnicht mehr alles nur durchleitet.
    
    /// <summary>
    /// Prüft ob eine Benutzername- & Passwortkombination gültig ist.
    /// </summary>
    public interface IUserAuthenficiation
    {
        Task<bool> Validate(LoginCredentials credentials);
        Task<bool> Add(LoginCredentials credentials);
        Task<bool> Remove(string username);
        Task<bool> DoesUsernameExist(string username);
        IEnumerable<User> All();
        Task<User> GetById(int id);
    }
}