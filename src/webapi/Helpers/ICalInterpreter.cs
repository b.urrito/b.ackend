using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Ical.Net;
using Ical.Net.CalendarComponents;

namespace Backend.WebApi.Helpers
{
    // TODO: es sollten hier keine Urls als Parameter übergeben werden sondern es sollte eine Instanz von etwas verlangt werden was direkt den Inhalt bereitstellt.
   
    public interface IICalInterpreter
    {
        Task<IEnumerable<CalendarEntry>> GetAll(string url);
        Task<IEnumerable<CalendarEntry>> GetForTodayAndTomorrow(string url);
        Task<IEnumerable<CalendarEntry>> GetAllFuture(string deviceCalendar);
    }
    
    public class ICalInterpreter : IICalInterpreter
    {
        private readonly IHttpDownloadHelper httpHelper;
        
        public ICalInterpreter(IHttpDownloadHelper httpHelper)
        {
            this.httpHelper = httpHelper;
        }
        
        public async Task<IEnumerable<CalendarEntry>> GetAll(string calendarUrl)
        {
            var calendar = await DeserializeCalendar(calendarUrl);
            var start = DateTime.MinValue;
            var end = DateTime.MaxValue;
            return GetFromStartToEnd(calendar, start, end);
        }

        public async Task<IEnumerable<CalendarEntry>> GetForTodayAndTomorrow(string calendarUrl)
        {
            var calendar = await DeserializeCalendar(calendarUrl);
            var start = DateTime.Today;
            var end = DateTime.Today + TimeSpan.FromDays(2) - TimeSpan.FromSeconds(1);
            return GetFromStartToEnd(calendar, start, end);
        }
        
        public async Task<IEnumerable<CalendarEntry>> GetAllFuture(string calendarUrl)
        {
            var calendar = await DeserializeCalendar(calendarUrl);
            var start = DateTime.Today;
            var end = DateTime.MaxValue;
            return GetFromStartToEnd(calendar, start, end);
        }

        private IEnumerable<CalendarEntry> GetFromStartToEnd(Calendar calendar, DateTime start, DateTime end)
        {
            var events = calendar
                .GetOccurrences(start, end)
                .Select(o => o.Source)
                .Cast<CalendarEvent>()
                .Distinct();
                
            var entries = events.Select(ConvertCalendarEvent);

            return entries;
        }

        private CalendarEntry ConvertCalendarEvent(CalendarEvent calendarEvent)
        {
            // Auf die Zeitzone muss keine Rücksicht genommen werden weil die Werte
            // Start.Value und End.Value sich auf UTC beziehen. Genau so werden die
            // Termine auch intern in der WebApi verwendet.
            return new CalendarEntry()
            {
                StartDateTime = calendarEvent.Start.Value,
                EndDateTime = calendarEvent.End.Value,
                Creator = "<unbekannt>",    // geschützte Information!
                Duration = (int)calendarEvent.Duration.TotalMinutes,
                Title = calendarEvent.Summary,
                Participants = calendarEvent.Attendees.Count
            };
        }

        private async Task<Calendar> DeserializeCalendar(string url)
        {
            var content = await this.httpHelper.ReadContentFromUrl(url);
            var calendar = Ical.Net.Calendar.Load(content);
            return calendar;
        }
    }
}