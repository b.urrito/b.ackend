using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Middleware welche die Authorisierung prüft. Wird für jeden Request aufgerufen.
    /// </summary>
    public class AuthentificationMiddleware
    {
        private readonly IList<string> UnprotectedRoutes = new List<string>
        {
            "api/users/login",
            "api/registrations/redeem"
        };
        private readonly RequestDelegate next;

        public AuthentificationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Methode welche für jeden Request aufgerufen wird.
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext, AppDbContext dbContext)
        {
            string authHeader = httpContext.Request.Headers["api_key"];
            
            // ---- DEBUG Hilfe ----
#if DEBUG
            if (string.IsNullOrWhiteSpace(authHeader) == false && authHeader == "aaaaaaaa")
            {
                await next.Invoke(httpContext);
                return;
            }
#endif
            // ----

            string path = httpContext.Request.Path.ToString().ToLower();
            if (UnprotectedRoutes.Any(s => s.Contains(path)) ||
                UnprotectedRoutes.Any(s => path.Contains(s)))
            {
                await next.Invoke(httpContext);
            }
            else if (string.IsNullOrWhiteSpace(authHeader))
            {
                httpContext.Response.StatusCode = 403;
                httpContext.Response.Body.Write(System.Text.Encoding.UTF8.GetBytes("Kein Authorisierungsheaderelement mitgeschickt."));
            }
            else if(dbContext.SessionTokens.Any(s => s.Value == authHeader && s.ValidTill > DateTime.Now))
            {
                await next.Invoke(httpContext);
            }
            else
            {
                httpContext.Response.StatusCode = 403;
                httpContext.Response.Body.Write(System.Text.Encoding.UTF8.GetBytes("Der Authorisierungstoken ist unbekannt."));
            }
        }
    }
}