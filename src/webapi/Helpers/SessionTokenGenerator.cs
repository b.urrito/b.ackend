using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Text;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Erzeugt einen Sessiontokenwert.
    /// </summary>
    public interface ISessionTokenGenerator
    {
        string Next();
    }

    /// <summary>
    /// Verwendet Kryptographiefunktionen um zufällige Bytes zu erzeugen aus denen ein String zusammengebaut wird.
    /// </summary>
    public class SystemSessionTokenGenerator : ISessionTokenGenerator
    {
        private readonly RNGCryptoServiceProvider cryptoProvider = new RNGCryptoServiceProvider();

        private readonly char[] ValidChars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        
        public string Next()
        {
            const int length = 24;
            var target = new byte[length];
            cryptoProvider.GetBytes(target, 0, length);
            
            for (int i = 0; i < length; ++i)
                target[i] = (byte) (target[i] % ValidChars.Length);

            return new string(target.ToList().Select(t => ValidChars[(int) t]).ToArray());
        }
    }
}