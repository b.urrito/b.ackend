using System.Threading.Tasks;
using Backend.WebApi.Repositories;

namespace Backend.WebApi.Helpers
{
    /// <summary>
    /// Entfernt alle abgelaufenen Auth- und SessionTokens.
    /// </summary>
    public interface ITokenCleaner
    {
        void CleanAll();
    }

    /// <summary>
    /// Entfernt alle abgelaufenen Auth- und SessionTokens.
    /// </summary>
    public class TokenCleaner : ITokenCleaner
    {
        private readonly IRegistrationsRepository registrationRepo;
        
        public TokenCleaner(IRegistrationsRepository registrationRepo)
        {
            this.registrationRepo = registrationRepo;
        }
        
        public void CleanAll()
        {
            // Wenn man die Registrierungen aufträumt werden die AuthTokens automatisch mit gesäubert.
            this.registrationRepo.CleanExpiredTokensSync();
        }
    }
}