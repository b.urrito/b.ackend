using System.Globalization;
using System.Linq;
using System.Text;

namespace Backend.WebApi.Helpers
{
    public class EspCharReplacer : IEspCharReplacer
    {
        public const int MaxStringLength = 71;
        private const string EllipsisSymbol = " [...]";

        public string ConvertToEspCompatible(string input)
        {
            var diacritiv = ConvertDiacritic(input);
            var shortened = ConvertToMaxLength(diacritiv, MaxStringLength);
            return shortened;
        }
        
        public string ConvertToMaxLength(string input, int maxLength)
        {
            if (input.Length > maxLength)
                input = input.Take(maxLength - EllipsisSymbol.Length) + EllipsisSymbol;
            return input;
        }
        
        public string ConvertDiacritic(string input)
        {
            input = input.Replace("ß", "ss");
            input = input.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(input[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(input[i]);
                }
            }
            input = sb.ToString().Normalize(NormalizationForm.FormC);
            return input;
        }
    }
}