using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;

namespace Backend.WebApi.Helpers
{
    public class PersistentUserAuthentification : IUserAuthenficiation
    {
        private readonly IUsersRepository usersRepository;
        private readonly ICryptoProvider cryptoProvider;
        
        public PersistentUserAuthentification(IUsersRepository usersRepository, ICryptoProvider cryptoProvider)
        {
            this.usersRepository = usersRepository;
            this.cryptoProvider = cryptoProvider;
        }
        
        public async Task<bool> Validate(LoginCredentials credentials)
        {
            var user = await this.usersRepository.GetByName(credentials.Username);
            if (user != null)
            {
                var givenPassword = this.cryptoProvider.HashPassword(credentials.Password, credentials.Username);
                return givenPassword == user.HashedPassword;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> Add(LoginCredentials credentials)
        {
            var user = new User
            {
                Name = credentials.Username,
                HashedPassword = cryptoProvider.HashPassword(credentials.Password, credentials.Username)
            };
            await this.usersRepository.Create(user);
            return true;
        }

        public async Task<bool> DoesUsernameExist(string username)
        {
            return await this.usersRepository.DoesUsernameExist(username);
        }

        public Task<bool> Remove(string username)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<User> All()
        {
            return this.usersRepository.GetAll();
        }

        public async Task<User> GetById(int id)
        {
            return await this.usersRepository.GetById(id);
        }
    }
}