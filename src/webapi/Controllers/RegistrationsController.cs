using System;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi.Helpers;
using Backend.WebApi.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Backend.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationsController : EspAwareController
    {
        private readonly IRegistrationsRepository registrationsRepository;
        private readonly IAuthTokenRepository authTokenRepository;
        private readonly IDevicesRepository deviceRepository;
        private readonly ISessionTokenRepository sessionRepository;
        private readonly IEspCharReplacer espCharReplacer;

        public RegistrationsController(
            IRegistrationsRepository registrationsRepository, 
            IAuthTokenRepository authTokenRepository, 
            IDevicesRepository deviceRepository, 
            ISessionTokenRepository sessionRepository,
            IEspCharReplacer espCharReplacer)
        {
            this.registrationsRepository = registrationsRepository;
            this.authTokenRepository = authTokenRepository;
            this.deviceRepository = deviceRepository;
            this.sessionRepository = sessionRepository;
            this.espCharReplacer = espCharReplacer;
        }

        [HttpPost]
        public async Task<IActionResult> CreateRegistrationToken([FromBody] Registration registration)
        {
            if (string.IsNullOrWhiteSpace(registration.Calendar) == false && registration.ValidateCalendarUrl())
            {
                if (string.IsNullOrWhiteSpace(registration.TimeZone))
                    registration.TimeZone = "Europe/Berlin";
                
                registration = await this.registrationsRepository.Persist(registration);
                var token = await this.authTokenRepository.CreateNewToken(registration);
                return new JsonResult(token);
            }
            else
            {
                return BadRequest("Die Kalender-URL ist ungültig.");
            }
        }

        [HttpDelete("invalidate/{token}")]
        public async Task<IActionResult> InvalidateRegistrationToken(int token)
        {
            var itemDeleted = await this.registrationsRepository.DeleteByToken(token);
            if (itemDeleted)
                return Ok();
            else
                return NotFound();
        }

        [HttpPost("redeem/{token}")]
        public async Task<IActionResult> RedeemRegistrationToken(int token)
        { 
            var registration = await this.registrationsRepository.GetRegistrationByAuthTokenValue(token);
            if (registration == null)
                return NotFound();
            
            var device = new Device
            {
                Calendar = registration.Calendar,
                Name = registration.Name,
                Location = registration.Location,
                LastActive = DateTime.Now,
                TimeZone = registration.TimeZone
            };
            device = await this.deviceRepository.Create(device);
            
            var deviceSessionToken = await this.sessionRepository.CreateNewDeviceToken(registration.Name + " / " + registration.Location);
            var bootstrap = new BootstrapInfo
            {
                DeviceId = device.Id,
                DeviceName = this.IsEsp() ? this.espCharReplacer.ConvertToEspCompatible(registration.Name) : registration.Name,
                DeviceLocation = this.IsEsp() ? this.espCharReplacer.ConvertToEspCompatible(registration.Location) : registration.Location,
                ApiKey = deviceSessionToken.Value,
                TimeZone = registration.TimeZone
            };

            await this.registrationsRepository.DeleteByToken(token);

            return new JsonResult(bootstrap);
        }
    }
}