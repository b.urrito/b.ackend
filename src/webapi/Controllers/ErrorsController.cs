using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;

namespace Backend.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorsController : Controller
    {
        private readonly IErrorsRepository errorsRepository;
        
        public ErrorsController(IErrorsRepository errorsRepository)
        {
            this.errorsRepository = errorsRepository;
        }
        
        [HttpGet]
        public JsonResult GetAll()
        {
            return new JsonResult(this.errorsRepository.GetAllErrors());
        }

        [HttpGet("{deviceId}")]
        public async Task<IActionResult> GetAllForDeviceById(Guid deviceId)
        {
            var errors = await this.errorsRepository.GetAllErrorsForDeviceById(deviceId);
            if (errors != null)
                return new JsonResult(errors);
            else
                return NotFound();
        }

        [HttpDelete("{deviceId}")]
        public async Task<IActionResult> DeleteErrorsForDeviceById(Guid deviceId)
        {
            var errorsDeleted = await this.errorsRepository.ResetErrorsForDeviceById(deviceId);
            if (errorsDeleted)
                return Ok();
            else
                return NotFound();
        }
        
    }
}