using System;
using System.Threading.Tasks;
using Backend.WebApi.Helpers;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Backend.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUserAuthenficiation authentification;
        private readonly ISessionTokenRepository tokenRepository;
        private readonly AppDbContext context;
        
        public UsersController(AppDbContext context, IUserAuthenficiation authentification, ISessionTokenRepository tokenRepository)
        {
            this.context = context;
            this.authentification = authentification;
            this.tokenRepository = tokenRepository;
        }
        
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginCredentials credentials)
        {
            if (await this.authentification.Validate(credentials))
            {
                var token = await this.tokenRepository.CreateNewUserToken(credentials.Username);
                return Ok(token.Value);
            }
            else
            {
                // BadRequest nicht Forbid weil Forbid immer darauf hinweist doch bitte eine Auth-Middleware
                // zu verwenden.
                return BadRequest("Ungültige Kombination aus Benutzername und Passwort.");
            }
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            var tokenValue = Request.Headers["api_key"];
            var tokenDeleted = await this.tokenRepository.RemoveTokenByValue(tokenValue);
            if (tokenDeleted)
                return Ok("Logout hat stattgefunden.");
            else
                return NotFound("Der Session Token wurde nicht gefunden.");
        }

        [HttpPost]
        public async Task<IActionResult> Create(LoginCredentials credentials)
        {
            var userAdded = await this.authentification.Add(credentials);
            if (userAdded)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPost("{name}/delete")]
        public async Task<IActionResult> Remove(string name)
        {
            var userRemoved = await this.authentification.Remove(name);
            if (userRemoved)
                return Ok();
            else
                return NotFound();
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            return new JsonResult(this.authentification.All());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var user = await this.authentification.GetById(id);
            if (user != null)
                return new JsonResult(user);
            else
                return NotFound();
        }
    }
}