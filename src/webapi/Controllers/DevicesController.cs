using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Backend.WebApi.Helpers;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.Internal;

namespace Backend.WebApi.Controllers
{
    public enum CalenderView
    {
        TwoDays,
        All,
        Future,
    }
    
    [Route("api/[controller]")]
    [ApiController]
    public class DevicesController : EspAwareController
    {
        private readonly IDevicesRepository deviceRepository;
        private readonly IICalInterpreter iCalInterpreter;
        private readonly IEspCharReplacer espCharReplacer;
        private readonly CalendarEntryFormatter _calendarEntryFormatter;

        public DevicesController(IDevicesRepository deviceRepository, IICalInterpreter iCalInterpreter, IEspCharReplacer espCharReplacer, CalendarEntryFormatter calendarEntryFormatter)
        {
            this.deviceRepository = deviceRepository;
            this.iCalInterpreter = iCalInterpreter;
            this.espCharReplacer = espCharReplacer;
            this._calendarEntryFormatter = calendarEntryFormatter;
        }

        [HttpGet("help")]
        public JsonResult GetHelp()
        {
            var methods = this.GetType().GetMethods().Where(m => m.IsPublic);
            var defaultMethods = typeof(Controller).GetMethods();

            var interestingMethods = methods.Where(m => defaultMethods.Any(n => AreMethodsIdentical(m, n)) == false);
            
            return new JsonResult(interestingMethods);
        }

        private bool AreMethodsIdentical(MethodInfo a, MethodInfo b)
        {
            return a.Name == b.Name &&
                   a.ReturnType == b.ReturnType &&
                   a.IsAbstract == b.IsAbstract &&
                   AreParameterInfosIdentical(a, b);
        }

        private bool AreParameterInfosIdentical(MethodInfo a, MethodInfo b)
        {
            var parametersA = a.GetParameters();
            var parametersB = b.GetParameters();

            return parametersA.All(p => parametersB.Any(q => IsParameterInfoIdentical(p, q)));
        }

        private bool IsParameterInfoIdentical(ParameterInfo a, ParameterInfo b)
        {
            return a.Name             == b.Name && 
                   a.IsOptional       == b.IsOptional && 
                   a.Position         == b.Position &&
                   a.ParameterType    == b.ParameterType;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var device = await this.deviceRepository.GetById(id);
            if(device != null)
                return new JsonResult(device);
            else
                return new NotFoundResult();
        }

        [HttpGet]
        public IActionResult GetAllDevices()
        {
            var devices = this.deviceRepository.GetAll() ?? new System.Collections.Generic.List<Device>();
            return new JsonResult(devices);
        }

        [HttpGet("{id}/calendar/events/{view?}")]
        public async Task<IActionResult> GetCalendar(Guid id, CalenderView view = CalenderView.TwoDays)
        {
            IEnumerable<CalendarEntry> entries;
            try
            {
                entries = await GetCalendarEntries(id, view);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            
            if (entries == null)
                return NotFound();
            else
                return new JsonResult(entries);
        }

        [HttpGet("{id}/calendar/prerenderedEvents/{view?}")]
        public async Task<IActionResult> GetPrerenderedCalendar(Guid id, CalenderView view = CalenderView.TwoDays)
        {
            // Device beziehen und in eine ESP-lesbare Instanz umwandeln.
            var device = await this.deviceRepository.GetById(id);
            if (device == null)
                return NotFound("Device id ist nicht bekannt.");
            
            IEnumerable<CalendarEntry> entries;
            try
            {
                entries = await GetCalendarEntries(id, view);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }

            if (entries == null)
            {
                return NotFound();
            }
            else
            {
                return this._calendarEntryFormatter.CreateLayout(
                                entries, 
                                device,
                                this.espCharReplacer.ConvertToEspCompatible);
            }
        }
        
        private async Task<IEnumerable<CalendarEntry>> GetCalendarEntries(Guid id, CalenderView view = CalenderView.TwoDays)
        {
            var device = await this.deviceRepository.GetById(id);
            if (device != null)
            {
                await this.deviceRepository.UpdateLastUsed(device);
                IEnumerable<CalendarEntry> entries = null;
                switch (view)
                {
                    case CalenderView.All:
                        Console.WriteLine("All");
                        entries = await this.iCalInterpreter.GetAll(device.Calendar);
                        break;
                    case CalenderView.Future:
                        Console.WriteLine("Future");
                        entries = await this.iCalInterpreter.GetAllFuture(device.Calendar);
                        break;
                    case CalenderView.TwoDays:
                        Console.WriteLine("TwoDays");
                        entries = await this.iCalInterpreter.GetForTodayAndTomorrow(device.Calendar);
                        break;
                    default:
                        throw new ArgumentException($"Kalender-View unbekannt: {view}.");
                }
                
                return entries;
            }
            else
                return null;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(Guid id)
        {
            var removedDevice = await this.deviceRepository.RemoveById(id);
            if(removedDevice)
                return Ok();
            else
                return NotFound();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, Device device)
        {
            if(device.Id == Guid.Empty)
                device.Id = id;
            if(device != null && device.CanBeUpdated())
            {
                var deviceUpdated = await this.deviceRepository.Update(device);
                if(deviceUpdated)
                    return Ok();
                else
                    return NotFound("Konnte kein Gerät mit dieser Id finden.");
            }
            else
            {
                return BadRequest("Die Gerätedefinition ist unbekannt, oder unvollständig.");
            }
        }
    }
}