using Microsoft.AspNetCore.Mvc;

namespace Backend.WebApi.Controllers
{
    public abstract class EspAwareController : Controller
    {
        /// <summary>
        /// Prüft anhand des User-Agent im Request ob es sich bei dem Gerät um einen ESP handetl.
        /// </summary>
        /// <returns></returns>
        protected bool IsEsp()
        {
            const string espUserAgent = "b.ildschirm esp8266";
            return this.Request.Headers["User-Agent"].ToString().ToLower() == espUserAgent;
        }
    }
}