Dockerfile
==========
Um den Container zu bauen und starten folgende Befehle verwenden:

	$ docker build -t burrito .
	$ docker run -it --rm -p 8080:80 --name myburrito burrito

Der Container wird dann interaktiv ausgeführt (man sieht die Logausgaben) und nach dem Beenden gelöscht.
	
