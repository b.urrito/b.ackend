using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi
{
    public class AppDbContext : DbContext
    {     
        internal virtual DbSet<AuthToken> AuthTokens { get; set; }
        internal virtual DbSet<Registration> Registrations { get; set; }
        internal virtual DbSet<Device> Devices { get; set; }
        internal virtual DbSet<User> Users { get; set; }
        internal virtual DbSet<SessionToken> SessionTokens { get; set; }
        internal virtual DbSet<Error> Errors { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthToken>().ToTable("AuthTokens");
            modelBuilder.Entity<AuthToken>().HasOne(rt => rt.Registration).WithOne(r => r.AuthToken)
                .HasForeignKey<AuthToken>(rt => rt.RegistrationId);
            
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<User>().HasIndex(u => u.Name).IsUnique();

            modelBuilder.Entity<Registration>().ToTable("Registrations");

            modelBuilder.Entity<SessionToken>().ToTable("SessionTokens");

            modelBuilder.Entity<Device>().ToTable("Devices");
            
            modelBuilder.Entity<Error>().ToTable("Errors");
            modelBuilder.Entity<Error>().HasOne<Device>(e => e.Device).WithMany(d => d.Errors)
                .HasForeignKey(e => e.DeviceId);
        }
    }
}