﻿using System;
using Backend.WebApi.Helpers;
using Backend.WebApi.Repositories;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Backend.WebApi
{
    public class Startup
    {
        private const string DatabaseFolder = "Database";
        private const string DatabaseName = "burrito.db";
        
        public IConfiguration Configuration { get; }
        
        private readonly IHostingEnvironment environment = null;
        
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            this.environment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {           
            if (this.environment.IsDevelopment())
            {
                //services.AddDbContext<AppDbContext>(options => options.UseInMemoryDatabase("TestingInMemoryDb"));
                services.AddDbContext<AppDbContext>(options => options.UseSqlite($"Filename=./{DatabaseFolder}/{DatabaseName}"));
            }
            else if (this.environment.IsProduction())
            {
                services.AddDbContext<AppDbContext>(options => options.UseSqlite($"Filename=./{DatabaseFolder}/{DatabaseName}"));
            }
            else
            {
                throw new InvalidOperationException("Environment ist weder 'Development' noch 'Production', es ist keine DB-Konfiguration für diesen Fall hinterlegt.");
            }
            
            services.AddHangfire(c => c.UseMemoryStorage());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(options => {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddSingleton<IHttpDownloadHelper, HttpDownloadHelper>();
            services.AddSingleton<IICalInterpreter, ICalInterpreter>();
            services.AddSingleton<IRandomAuthTokenGenerator, SystemRandomAuthTokenGenerator>();
            services.AddSingleton<IUserAuthenficiation, StaticUserAuthentification>();
            services.AddTransient<IAuthTokenRepository, AuthTokenRepository>();
            services.AddSingleton<ISessionTokenGenerator, SystemSessionTokenGenerator>();
            services.AddTransient<ISessionTokenRepository, SessionTokenRepository>();
            services.AddTransient<IRegistrationsRepository, RegistrationsRepository>();
            services.AddTransient<IDevicesRepository, DevicesRepository>();
            services.AddTransient<IUsersRepository, UsersRepository>();
            services.AddSingleton<ICryptoProvider, CryptoProvider>();
            services.AddTransient<IErrorsRepository, ErrorsRepository>();
            services.AddTransient<ITokenCleaner, TokenCleaner>();
            services.AddTransient<IDeviceActivityMonitor, DeviceActivityMonitor>();
            services.AddSingleton<IEspCharReplacer, EspCharReplacer>();
            services.AddSingleton<CalendarEntryFormatter, JsonCalendarEntryFormatter>();
        }

        public void ConfigureCrons(ITokenCleaner cleaner, IDeviceActivityMonitor activityMonitor)
        {
            const int TokenCleanIntervalInMinutes = 60;
            const int ActivityMonitorIntervalInMinutes = 30;

            RecurringJob.AddOrUpdate(
                () => cleaner.CleanAll(), Cron.MinuteInterval(TokenCleanIntervalInMinutes)
            );
            
            RecurringJob.AddOrUpdate(
                () => activityMonitor.CheckDevicesForActivity(), Cron.MinuteInterval(ActivityMonitorIntervalInMinutes)
            );
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AppDbContext context, ITokenCleaner tokenCleaner, IDeviceActivityMonitor activityMonitor)
        {
            if (System.IO.Directory.Exists($"./{DatabaseFolder}") == false)
                System.IO.Directory.CreateDirectory($"./{DatabaseFolder}");
            context.Database.EnsureCreated();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMiddleware<AuthentificationMiddleware>();
            
            // TODO: Https Redirect wieder einschalten sobald der Dienst auf einer echten Domain läuft.
            // app.UseHttpsRedirection();
            app.UseMvc();
            app.UseHangfireServer();

            ConfigureCrons(tokenCleaner, activityMonitor);
        }
    }
}
