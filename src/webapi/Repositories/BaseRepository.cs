namespace Backend.WebApi.Repositories
{
    /// <summary>
    /// Basisklasse für alle Repositories.
    /// </summary>
    public abstract class BaseRepository
    {
        protected AppDbContext Context { get; }

        public BaseRepository(AppDbContext context)
        {
            this.Context = context;
        }
    }
}