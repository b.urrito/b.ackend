using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Repositories
{
    public class ErrorsRepository : BaseRepository, IErrorsRepository
    {
        public ErrorsRepository(AppDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Persistiert einen Fehler in der Datenbank.
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public async Task<bool> Create(Error error)
        {
            if (error != null && error.IsMinimalDataSet())
            {
                if(error.Timestamp == default(DateTime))
                    error.Timestamp = DateTime.Now;

                await this.Context.Errors.AddAsync(error);
                await this.Context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Persistiert einen Satz an Fehlern.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public async Task<bool> CreateAll(IEnumerable<Error> errors)
        {
            if (errors.All(e => e.IsMinimalDataSet()))
            {
                foreach(var error in errors)
                    if(error.Timestamp == default(DateTime))
                        error.Timestamp = DateTime.Now;

                await this.Context.Errors.AddRangeAsync(errors);
                await this.Context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateAllSync(IEnumerable<Error> errors)
        {
            if(errors.All(e => e.IsMinimalDataSet()))
            {
                foreach(var error in errors)
                    if(error.Timestamp == default(DateTime))
                        error.Timestamp = DateTime.Now;
                
                this.Context.Errors.AddRange(errors);
                this.Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        
        /// <summary>
        /// Läst alle Fehler aller Geräte aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public IEnumerable<Error> GetAllErrors()
        {
            return this.Context.Errors.Include(e => e.Device);
        }

        /// <summary>
        /// Lädt alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public IEnumerable<Error> GetAllErrorsForDevice(Device device)
        {
            if (device == null)
                return new List<Error>();

            return this.Context.Errors.Include(e => e.Device).Where(e => e.DeviceId == device.Id);
        }

        /// <summary>
        /// Lädt alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Error>> GetAllErrorsForDeviceById(System.Guid deviceId)
        {
            var device = await this.Context.Devices.FindAsync(deviceId);
            if (device != null)
            {
                return GetAllErrorsForDevice(device);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Löscht alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task<bool> ResetErrorsForDevice(Device device)
        {
            if (device == null)
                return false;
            else
                return await ResetErrorsForDeviceById(device.Id);
        }

        /// <summary>
        /// Löscht alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task<bool> ResetErrorsForDeviceById(System.Guid id)
        {
            if (id == System.Guid.Empty)
                return false;
            
            var toRemove = this.Context.Errors.Where(e => e.DeviceId == id);
            this.Context.RemoveRange(toRemove);
            await this.Context.SaveChangesAsync();
            return true;
        }
    }
}