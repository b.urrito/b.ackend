using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Repositories
{
    /// <summary>
    /// Regelt die Erstellung von Einlösung von Registrierungen (und AuthTokens).
    /// </summary>
    /// <typeparam name="int"></typeparam>
    /// <typeparam name="AuthToken"></typeparam>
    public class AuthTokenRepository : BaseTokenRepository<int, AuthToken>, IAuthTokenRepository
    {
        private readonly IRandomAuthTokenGenerator _tokenGenerator;

        public AuthTokenRepository(AppDbContext context, IRandomAuthTokenGenerator tokenGenerator)
            : base(context)
        {
            this._tokenGenerator = tokenGenerator;
        }
        
        /// <summary>
        /// Erzeugt einen neuen AuthToken für die übergebene Registrierung.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public async Task<AuthToken> CreateNewToken(Registration registration)
        {
            var token = new AuthToken
            {
                Value = CreateUnusedTokenValue(),
                GenerationDate = DateTime.Now,
                ExpirationDate = DateTime.Now + TimeSpan.FromMinutes(AuthToken.DefaultLifeTimeInMinutes),
                Registration = registration,
                RegistrationId = registration.Id
            };

            await this.Context.AuthTokens.AddAsync(token);
            await this.Context.SaveChangesAsync();
            
            return token;
        }
        
        /// <summary>
        /// Prüft ob der übergebene Wert als Wert eines Tokens in der DB existiert.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected override bool DoesTokenValueExist(int token)
        {
            return this.Context.AuthTokens.Any(x => x.Value == token);
        }

        /// <summary>
        /// Für das Erzeugen neuer Tokens notwendige Funktion (Eindeutigkeit).
        /// Liefert den tatsächlichen Wert der Token-Instanz zurück.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected override int GetValueFromToken(AuthToken token)
        {
            return token.Value;
        }

        /// <summary>
        /// Für das Erzeugen neuer Tokens notwendige Funktion (Eindeutigkeit).
        /// Prüft, ob die Datenbank einen Token mit dem übergebenen Wert enthält. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override async Task<AuthToken> GetTokenByValue(int value)
        {
            return await this.Context.AuthTokens.FirstOrDefaultAsync(t => t.Value == value);
        }

        /// <summary>
        /// Erzeugt (ohne Validierungen) einen neuen Tokenwert (keinen ganzen Token).
        /// </summary>
        /// <returns></returns>
        protected override int Create()
        {
            return this._tokenGenerator.Next();
        }
    }
}