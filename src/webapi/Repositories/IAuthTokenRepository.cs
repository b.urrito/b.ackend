using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Repositories
{
    public interface IAuthTokenRepository
    {
        /// <summary>
        /// Erzeugt einen neuen AuthToken für die übergebene Registrierung.
        /// Setzt in der Registrierung dann auch das Token und persistiert dies.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<AuthToken> CreateNewToken(Registration registration);
    }
}