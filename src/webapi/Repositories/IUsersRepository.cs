using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Repositories
{
    public interface IUsersRepository
    {
        Task<User> Create(User user);
        Task<bool> Remove(User user);
        Task<bool> RemoveById(Guid id);
        Task<bool> RemoveByName(string name);
        Task<User> GetById(int id);
        Task<User> GetByName(string name);
        IEnumerable<User> GetAll();
        Task<bool> DoesUsernameExist(string username);
    }

    public class UsersRepository : BaseRepository, IUsersRepository
    {
        public UsersRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<User> Create(User user)
        {
            if (user != null && user.IsMinimalDataSet())
            {
                await this.Context.Users.AddAsync(user);
                await this.Context.SaveChangesAsync();
                return user;
            }
            else
            {
                throw new ArgumentException("Es sind nicht alle nötigen Felder gesetzt um den Nutzer in der DB persistieren zu können.");
            }
        }

        public async Task<bool> Remove(User user)
        {
            if (user == null)
                return false;
            
            this.Context.Users.Remove(user);
            await this.Context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> RemoveById(Guid id)
        {
            var user = await this.Context.Users.FindAsync(id);
            return await Remove(user);
        }

        public async Task<bool> RemoveByName(string name)
        {
            var user = await this.Context.Users.FirstOrDefaultAsync(u => u.Name == name);
            return await Remove(user);
        }

        public Task<User> GetById(int id)
        {
            return this.Context.Users.FindAsync(id);
        }

        public Task<User> GetByName(string name)
        {
            return this.Context.Users.FirstOrDefaultAsync(u => u.Name == name);
        }

        public IEnumerable<User> GetAll()
        {
            return this.Context.Users;
        }

        public Task<bool> DoesUsernameExist(string username)
        {
            return this.Context.Users.AnyAsync(u => u.Name == username);
        }
    }
}