using System;
using System.Threading.Tasks;

namespace Backend.WebApi.Repositories
{
    /// <summary>
    /// Basisklasse für alle Token-Repositories.
    /// </summary>
    /// <typeparam name="TTokenValue"></typeparam>
    /// <typeparam name="TToken"></typeparam>
    public abstract class BaseTokenRepository<TTokenValue, TToken> : BaseRepository where TToken: class 
    {
        public BaseTokenRepository(AppDbContext context)
            : base(context)
        { }
        
        /// <summary>
        /// Zur Erzeugung neuer Tokens notwendige Funktion welche prüft ob ein Wert bereits existiert.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected abstract bool DoesTokenValueExist(TTokenValue token);

        /// <summary>
        /// Zur Erzeugung neuer Tokens notwendige Funktion welche einen neuen Tokenwert (nicht Tokeninstanz) erzeugt.
        /// </summary>
        /// <returns></returns>
        protected abstract TTokenValue Create();
        
        /// <summary>
        /// Erzeugt einen neuen, eindeutigen Tokenwert.
        /// </summary>
        /// <returns></returns>
        protected TTokenValue CreateUnusedTokenValue()
        {
            // Diese Lösung ist nicht perfekt, aber da wir nicht mir sehr vielen Anfragen rechnen sollte sie
            // die schnellste und einfachste sein. Besser wäre es zusätzlich vorzuhalten welche Nummern frei
            // sind und eine zugällige daraus zu ziehen.
            int maxTries = 1000;
            int fallbackCounter = 0;
            TTokenValue value = Create();
            while (DoesTokenValueExist(value))
            {
                value = Create();
                fallbackCounter++;
                if (fallbackCounter >= maxTries)
                    throw new InvalidOperationException(
                        $"Nach {maxTries} Versuchen konnte kein freier Token bestimmt werden.");
            }

            return value;
        }

        /// <summary>
        /// Für das Erstellen neuer Tokens notwendige Funktion welche den Wert eines Tokens liefrrt.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected abstract TTokenValue GetValueFromToken(TToken token);

        /// <summary>
        /// Für das Erstellen neuer Tokens notwendige Funktion welche einen Token anhand eines Tokenwerts findet.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected abstract Task<TToken> GetTokenByValue(TTokenValue value);

        /// <summary>
        /// Löscht einen Token anhand seines Werts.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<bool> RemoveTokenByValue(TTokenValue value)
        {
            var token = await GetTokenByValue(value);
            if (token != null)
            {
                await RemoveToken(token);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Löscht einen Token.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> RemoveToken(TToken token)
        {
            if(DoesTokenValueExist(GetValueFromToken(token)))
            {
                this.Context.Remove(token);
                await this.Context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}