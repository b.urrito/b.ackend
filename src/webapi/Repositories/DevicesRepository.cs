using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Backend.SharedEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Repositories
{
    /// <summary>
    /// Abstraktion des Gerätepersistenzlayers.
    /// </summary>
    public class DevicesRepository : BaseRepository, IDevicesRepository
    {
        public DevicesRepository(AppDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Persistiert das übergebene Gerät in der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task<Device> Create(Device device)
        {
            if (device.IsMinimalDataSet())
            {
                await this.Context.Devices.AddAsync(device);
                await this.Context.SaveChangesAsync();
                return device;
            }
            else
            {
                throw new ArgumentException("Es sind nicht alle nötigen Felder gesetzt um das Gerät in der DB zu speichern.");
            }
        }

        /// <summary>
        /// Lädt ein Gerät anhand seiner Id aus der Datenbank.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Device> GetById(Guid id)
        {
            var device = await this.Context.Devices.Include(d => d.Errors).SingleOrDefaultAsync(d => d.Id == id);
            return device;
        }

        /// <summary>
        /// Lädt alle Geräte aus de Datenbank.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Device> GetAll()
        {
            return this.Context.Devices.Include(d => d.Errors);
        }

        /// <summary>
        /// Löscht ein Gerät anhand seiner Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true wenn ein Gerät gefunden und gelöscht wurde, ansonsten false</returns>
        public async Task<bool> RemoveById(Guid id)
        {
            var device = await this.Context.Devices.FindAsync(id);
            if(device != null)
            {
                this.Context.Devices.Remove(device);
                await this.Context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Prüft ob ein Device in der Datenbank vorhanden ist und
        /// aktualisiert dann die Konfiguration.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task<bool> Update(Device device)
        {
            var propertyNameBlackList = new List<string>()
            {
                nameof(Device.LastActive)
            };

            var oldDevice = await this.Context.Devices.FindAsync(device.Id);
            if(device != null)
            {
                foreach(var property in typeof(Device).GetProperties()
                                                      .Where(p => propertyNameBlackList.Contains(p.Name) == false)
                                                      .Where(p => p.CanWrite))
                {
                    var propertyName = property.Name;
                    var propertyType = property.PropertyType;
                    bool isDefault = false;

                    Console.WriteLine($"Datenabgleich, Name: {propertyName}, Typ: {propertyType}");

                    if(propertyType.IsValueType)
                        isDefault = property.GetValue(device) == Activator.CreateInstance(propertyType);
                    else
                        isDefault = property.GetValue(device) == null;

                    if(isDefault == false)
                    {
                        property.SetValue(oldDevice, property.GetValue(device, null));
                    }
                }

                this.Context.Devices.Update(oldDevice);
                await this.Context.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Setzt den letzten Abruf des übergebenen Geräts auf den aktuellen Zeitpunkt.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task UpdateLastUsed(Device device)
        {
            device.LastActive = DateTime.Now;
            this.Context.Devices.Update(device);
            await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Ruft alle Geräte ab welche sich seit <paramref name="nminutes"/> Minuten nicht mehr gemeldet haben.
        /// </summary>
        /// <param name="nminutes"></param>
        /// <returns></returns>
        public IEnumerable<Device> GetInactiveFor(int minutes)
        {
            var inactivityThreshold = DateTime.Now - TimeSpan.FromMinutes(minutes);
            var inactiveDevices = this.Context.Devices.Where(d => d.LastActive <=inactivityThreshold);
            return inactiveDevices;
        }
    }
}