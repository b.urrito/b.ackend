using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Backend.WebApi.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Helpers
{
    public class SessionTokenRepository
        : BaseTokenRepository<string, SessionToken>, ISessionTokenRepository
    {
        private readonly ISessionTokenGenerator tokenGenerator;

        public SessionTokenRepository(AppDbContext context, ISessionTokenGenerator tokenGenerator)
            : base(context)
        {
            this.tokenGenerator = tokenGenerator;
        }

        /// <summary>
        /// Erzeugt einen neuen Sessiontoken für den entsprechenden Nutzer.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        private async Task<SessionToken> CreateNewToken(string username, DateTime validTill)
        {
            var token = new SessionToken()
            {
                GenerationDate = DateTime.Now,
                GeneratedFor = username,
                ValidTill = validTill,
                Value = CreateUnusedTokenValue()
            };

            await this.Context.SessionTokens.AddAsync(token);
            await this.Context.SaveChangesAsync();
            
            return token;
        }

        /// <summary>
        /// Erzeugt einen neuen SessionToken für einen Benutzer. Diese haben eine begrenzte Lebensdauer.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<SessionToken> CreateNewUserToken(string username)
        {
            return await CreateNewToken(username, DateTime.Now + TimeSpan.FromDays(14));
        }

        /// <summary>
        /// Erzeugt einen neuen SessionToken für ein Gerät. Diese haben eine praktisch unbegrenzte Lebensdauer.
        /// </summary>
        /// <param name="deviceName"></param>
        /// <returns></returns>
        public async Task<SessionToken> CreateNewDeviceToken(string deviceName)
        {
            return await CreateNewToken(deviceName, DateTime.MaxValue);
        }

        /// <summary>
        /// Für das Erzeugen neuer Tokens notwendige Funktion (Eindeutigkeit).
        /// Liefert den tatsächlichen Wert der Token-Instanz zurück.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected override string GetValueFromToken(SessionToken token)
        {
            return token.Value;
        }

        /// <summary>
        /// Für das Erzeugen neuer Tokens notwendige Funktion (Eindeutigkeit).
        /// Prüft, ob die Datenbank einen Token mit dem übergebenen Wert enthält. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override async Task<SessionToken> GetTokenByValue(string value)
        {
            return await this.Context.SessionTokens.FirstOrDefaultAsync(t => t.Value == value);
        }

        /// <summary>
        /// Prüft ob der übergebene Wert als Wert eines Tokens in der DB existiert.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected override bool DoesTokenValueExist(string token)
        {
            return this.Context.SessionTokens.Any(t => t.Value == token);
        }

        /// <summary>
        /// Erzeugt einen neuen Tokenwert (ohne Validierung).
        /// </summary>
        /// <returns></returns>
        protected override string Create()
        {
            return this.tokenGenerator.Next();
        }
    }
}