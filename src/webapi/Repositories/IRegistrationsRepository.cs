using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Repositories
{
    public interface IRegistrationsRepository
    {
        /// <summary>
        /// Speichert die übergebene Registrierung in der Datenbank.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<Registration> Persist(Registration registration);
        /// <summary>
        /// Löscht eine Registrierung anhand ihres Authtoken-Werts.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> DeleteByToken(int token);
        /// <summary>
        /// Lädt eine Registrierung aus der Datenbank anhand des Werts des zugehörigen AuthTokens.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<Registration> GetRegistrationByAuthTokenValue(int value);
        /// <summary>
        /// Löscht alle abgelaufenen Tokens/Registrierungen.
        /// </summary>
        /// <returns></returns>
        void CleanExpiredTokensSync();
    }
}