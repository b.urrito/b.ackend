using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.WebApi.Repositories
{
    public class RegistrationsRepository : BaseRepository, IRegistrationsRepository
    {
        public RegistrationsRepository(AppDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Speichert die übergebene Registrierung in der Datenbank.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public async Task<Registration> Persist(Registration registration)
        {
            if (registration.IsMinimalDataSet())
            {
                await this.Context.Registrations.AddAsync(registration);
                await this.Context.SaveChangesAsync();
                return registration;
            }
            else
            {
                throw new ArgumentException("Für die Registrierung sind nicht alle notwendigen Daten gesetzt");
            }
        }

        /// <summary>
        /// Löscht eine Registrierung anhand ihres Authtoken-Werts.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByToken(int token)
        {
            var authToken = await this.Context.AuthTokens
                                              .Include(a => a.Registration)
                                              .FirstOrDefaultAsync(a => a.Value == token);
            if (authToken != null && authToken.Registration == null)
            {
                throw new InvalidOperationException("Das Registrierungs-Feld eines AuthTokens ist null.");
            }
            else if (authToken != null && authToken.ExpirationDate < DateTime.Now)
            {
                this.Context.AuthTokens.Remove(authToken);
                this.Context.Registrations.Remove(authToken.Registration);
                await this.Context.SaveChangesAsync();
                throw new ArgumentException("Der übergeben Token ist nicht mehr gültig. Registrierung und AuthToken wurden gelöscht.");
            }
            else if (authToken != null)
            {
                this.Context.AuthTokens.Remove(authToken);
                this.Context.Registrations.Remove(authToken.Registration);
                await this.Context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Lädt eine Registrierung aus der Datenbank anhand des Werts des zugehörigen AuthTokens.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<Registration> GetRegistrationByAuthTokenValue(int value)
        {
            var authToken = await this.Context.AuthTokens.Include(a => a.Registration).ThenInclude(r => r.AuthToken).FirstOrDefaultAsync(a => a.Value == value);
            if (authToken == null)
                return null;
            else
                return authToken.Registration;
        }

        /// <summary>
        /// Löscht alle abgelaufenen AuthTokens (und deren Registrierungen).
        /// </summary>
        /// <returns></returns>
        public void CleanExpiredTokensSync()
        {
            var expired = this.Context.AuthTokens.Include(a => a.Registration).Where(a => a.ExpirationDate <= DateTime.Now);
            foreach (var token in expired)
            {
                this.Context.AuthTokens.Remove(token);
                this.Context.Registrations.Remove(token.Registration);
            }

            Context.SaveChanges();
        }
    }
}