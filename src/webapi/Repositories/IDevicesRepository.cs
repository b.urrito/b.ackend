using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Repositories
{
    public interface IDevicesRepository
    {
        /// <summary>
        /// Persistiert das übergebene Gerät in der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<Device> Create(Device device);
        /// <summary>
        /// Lädt ein Gerät anhand seiner Id aus der Datenbank.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Device> GetById(Guid id);
        /// <summary>
        /// Lädt alle Geräte aus de Datenbank.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Device> GetAll();
        /// <summary>
        /// Löscht ein Gerät anhand seiner Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true wenn ein Gerät gefunden und gelöscht wurde, ansonsten false</returns>
        Task<bool> RemoveById(Guid id);
        /// <summary>
        /// Prüft ob ein Device in der Datenbank vorhanden ist und
        /// aktualisiert dann die Konfiguration.
        /// </summary>
        /// <param name="device"></param>
        /// <returns>true wenn ein Update durchgeführt wurde, ansonsten false</returns>
        Task<bool> Update(Device device);
        /// <summary>
        /// Setzt den letzten Abruf des übergebenen Geräts auf den aktuellen Zeitpunkt.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task UpdateLastUsed(Device device);
        /// <summary>
        /// Ruft alle Geräte ab welche sich seit <paramref name="nminutes"/> Minuten nicht mehr gemeldet haben.
        /// </summary>
        /// <param name="nminutes"></param>
        /// <returns></returns>
        IEnumerable<Device> GetInactiveFor(int nminutes);
    }
}