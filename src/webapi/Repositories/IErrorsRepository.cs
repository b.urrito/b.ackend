using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Repositories
{
    public interface IErrorsRepository
    {
        /// <summary>
        /// Persistiert einen Fehler in der Datenbank.
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        Task<bool> Create(Error error);

        /// <summary>
        /// Persistiert einen Satz an Fehlern.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        Task<bool> CreateAll(IEnumerable<Error> errors);

        /// <summary>
        /// Persistiert einen Satz an Fehlern synchron.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        bool CreateAllSync(IEnumerable<Error> errors);

        /// <summary>
        /// Läst alle Fehler aller Geräte aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        IEnumerable<Error> GetAllErrors();

        /// <summary>
        /// Lädt alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        IEnumerable<Error> GetAllErrorsForDevice(Device device);

        /// <summary>
        /// Lädt alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task<IEnumerable<Error>> GetAllErrorsForDeviceById(System.Guid deviceId);

        /// <summary>
        /// Löscht alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<bool> ResetErrorsForDevice(Device device);

        /// <summary>
        /// Löscht alle Fehler eines bestimmten Geräts aus der Datenbank.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<bool> ResetErrorsForDeviceById(System.Guid id);
    }
}