using System.Threading.Tasks;
using Backend.SharedEntities.Models;

namespace Backend.WebApi.Repositories
{
    public interface ISessionTokenRepository
    {
        /// <summary>
        /// Erzeugt einen neuen Sessiontoken für den entsprechenden Nutzer.
        /// Die Lebensdauer dieses Tokens ist begrenzt.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<SessionToken> CreateNewUserToken(string username);
        /// <summary>
        /// Erzeugt einen neuen Sessiontoken für das Gerät.
        /// Die Lebensdauer dieses Tokens ist praktisch unbegrenzt.
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<SessionToken> CreateNewDeviceToken(string username);
        /// <summary>
        /// Löscht einen Sessiontoken.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> RemoveToken(SessionToken token);
        /// <summary>
        /// Löscht einen Sessiontoken anhand seines Werts.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<bool> RemoveTokenByValue(string value);
    }
}