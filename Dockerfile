FROM microsoft/dotnet:2.1-sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln ./
COPY ./src/shared_entities/ ./src/shared_entities/
COPY ./src/webapi/ ./src/webapi/
COPY ./tests/unit_tests/ ./tests/unit_tests/
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/src/webapi/out .
ENTRYPOINT ["dotnet", "WebApi.dll"]
